# Bitmap

A command line program written in C that I wrote for an assignment for the Advanced Programming class. It is a very simple image editor that can only perform two actions: crop images and rotate images.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

- **Visual Studio**: The IDE used to develop the program. Can be installed from https://www.visualstudio.com/.
- **Desktop development with C++**: A workload you can install into Visual Studio that allows you to write C/C++ code in Visual Studio. You can install it by opening the "Visual Studio Installer" program (comes with Visual Studio), clicking on the "More > Modify" button for the version of Visual Studio you installed, and checking the "Desktop development with C++" checkbox under the "Windows" section.

### Installing

Ensure that you have installed Visual Studio with the "Desktop development with C++" workload onto your machine. When finished, you can open the "Bitmap.sln" file and run the program with "Ctrl + F5."

Note: If you get compiler errors when trying to run the program, you may need to retarget the project's SDK by right clicking the "Bitmap" project in the "Solution Explorer", clicking on "Retarget Projects," choosing a Windows SDK that's installed on your machine, and clicking OK. After doing so, you may need to right click the "Bitmap" project again and click on "Rescan Solution." This should eliminate the compiler errors. 

### Usage

The program is very easy to use: you just need to enter the letter 'C' if you want to crop or 'R' if you want to rotate.

In both cases, you will need to enter the name of the image file you want to crop/rotate and the filename for the newly-modified image. The program assumes that the images are in the same location as the program itself, or in the same location that ".c" files are in if you are running the program through Visual Studio.

Note that the only images accepted by this program are 24-bit bitmap (.bmp) files that aren't compressed or encrypted, and the images must not contain an alpha channel.

If cropping, you just specify the coordinates for the top-left and bottom-right corners of the crop. All coordinates are relative to the top-left corner of the original image. If you specify a coordinate out of bounds, the program automatically readjusts it to be within bounds (at the edge of the image).

If rotating, you just specify how many degrees you want to rotate the image by clockwise.

In the end, a new 24-bit bitmap file should be created with the name you specified earlier with the modifications you specified.

## Built With

* [Visual Studio](https://www.visualstudio.com/) - The IDE used to make the program.

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)


> Written with [StackEdit](https://stackedit.io/).