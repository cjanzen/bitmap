#include "bitmap.h"
#include <stdio.h>
#include <ctype.h>

#define M_PI 3.14159265358979323846
/*
Author: Corey Janzen
Date: Apr 11, 2018
*/

/* Crops the image using the user-specified coordinates for the image */
void CropImage()
{
	// Coordinates for the crop
	int iTop = 0, iLeft = 0, iBottom = 0, iRight = 0;
	
	// Ask the user for an input image file
	FILE* infile = GetFile("Enter the name of the image file you want to crop", "rb");
	// Ask the user for an output image file
	FILE* outfile = GetFile("Enter the filename that you want to save the cropped image to", "w+b");

	// Ask for coordinates
	printf("Enter the coordinates (in pixels) that you want to crop:\n");
	printf("Top: ");
	scanf("%d", &iTop);
	printf("Left: ");
	scanf("%d", &iLeft);
	printf("Bottom: ");
	scanf("%d", &iBottom);
	printf("Right: ");
	scanf("%d", &iRight);

	// Create an image for containing the original image
	IMAGE oldImg = { NULL, NULL };

	// Read in the old image
	ReadImage(&oldImg, infile);
	fclose(infile);

	// Crop the image, save it to a new image
	IMAGE newImg = Crop(&oldImg, iTop, iLeft, iBottom, iRight);

	// Save the new image to a file
	WriteImage(&newImg, outfile);
	if (outfile != NULL)
	{
		fclose(outfile);
	}

	// Free both the old image and the new image
	FreeImage(&oldImg);
	FreeImage(&newImg);
}

// Rotates the image with by the user-specified degrees
void Rotate()
{
	// How much you want to rotate the image in clockwise (in degrees)
	float theta = 0.0f;

	// Ask the user for an input image file
	FILE* infile = GetFile("Enter the name of the image file you want to rotate", "rb");
	// Ask the user for an output image file
	FILE* outfile = GetFile("Enter the filename that you want to save the rotated image to", "w+b");

	// Ask for rotation in degrees, then convert to radians
	printf("Enter the number of degrees to rotate by clockwise:\n");
	scanf("%f", &theta);
	theta = theta * (M_PI/180);

	// Create an image for containing the original image
	IMAGE oldImg = { NULL, NULL };

	// Read in the old image
	ReadImage(&oldImg, infile);
	fclose(infile);

	// Crop the image, save it to a new image
	IMAGE newImg = RotateImage(&oldImg, theta);

	// Save the new image to a file
	WriteImage(&newImg, outfile);
	if (outfile != NULL)
	{
		fclose(outfile);
	}

	// Free both the old image and the new image
	FreeImage(&oldImg);
	FreeImage(&newImg);
}

void main()
{
	// Holds the action the user wants to do
	char cAction = '\0';
	
	// Ask the user what they want to do
	printf("What do you want to do?\n");
	printf("\t[C]rop the image.\n");
	printf("\t[R]otate the image the image.\n");

	// While the user hasn't made a valid choice
	while (cAction != 'C' && cAction != 'R')
	{
		// Get input and uppercase it
		scanf("%c", &cAction);
		cAction = toupper(cAction);

		// Clear input buffer
		fseek(stdin, 0, SEEK_END);

		// Check if the input is valid
		if (cAction != 'C' && cAction != 'R')
		{
			// If not valid, print error message
			printf("%c is not a valid option.\n", cAction);
		}
	}

	// Perform an action based on the choice made
	switch (cAction)
	{
	case 'C':
		CropImage();
		break;
	case 'R':
		Rotate();
		break;
	default:
		printf("The action specified hasn't been implemented.\n");
		break;
	}
}


