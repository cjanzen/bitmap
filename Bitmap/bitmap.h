#ifndef BITMAP_H
#define BITMAP_H
#include <stdio.h>
/*
Author: Corey Janzen
Date: Apr 11, 2018
*/

// This image editor is for 24 bit images, no compression, alpha channel, encryption
#define BOOL unsigned char
#define TRUE 1
#define FALSE 0
#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned int
#define BMPHDRSIZE 54
#define PIXELSIZE 3

// A struct to represent the header information
#pragma pack (push)
#pragma pack (1)
typedef struct
{
	WORD wType;
	DWORD dwFileSize;
	WORD wReserved1;
	WORD wReserved2;
	DWORD dwDataOffset;
	DWORD dwHeaderSize;
	DWORD dwWidth;
	DWORD dwHeight;
	WORD wPlanes;
	WORD wBitCount;
	DWORD dwCompression;
	DWORD dwImageSize;
	DWORD dwXPelsPerMeter;
	DWORD dwYPelsPerMeter;
	DWORD dwClrUsed;
	DWORD dwClrImportant;
}BITMAPHDR;

// A struct to represent an BGR pixel
typedef struct
{
	BYTE bBlue, bGrn, bRed;
}PIXEL;

// A struct to represent an iamge
typedef struct
{
	BITMAPHDR* bmHDR;
	PIXEL* bmData;
}IMAGE;

// Function prototypes
/* Gets a file handle from the user */
FILE* GetFile(char* cPrompt, char* cMode);

// Functions to read in from a bitmap
void ReadImage(IMAGE* imgPtr, FILE* infile);
void ReadHeader(IMAGE* imgPtr, FILE* infile);
void ReadData(IMAGE* imgPtr, FILE* infile);

// Functions to write data to a file
void WriteImage(IMAGE* imgPtr, FILE* outfile);
void WriteHeader(IMAGE* imgPtr, FILE* outfile);
void WriteData(IMAGE* imgPtr, FILE* outfile);

/* Free's Bitmap's inner members */
void FreeImage(IMAGE * img);

/* Retrieves a reference to a single pixel in the image */
PIXEL* GetPixel(unsigned int row, unsigned int col, IMAGE* imgPtr);

//Assignment 3
/*
Crops the specified image using the specified coordinates and returns
a new cropped image.
- IMAGE * imgPtr: Image you want to crop
- int iTop: position from the top of the image
- int iLeft: Position from the left of the image
- int iBottom: position from the top of the image where the image will
be cut off (should be greater than iTop)
- int iRight: position from the left of the image where the image will
be cut off (should be greater than iLeft)
*/
IMAGE Crop(IMAGE * imgPtr, int iTop, int iLeft, int iBottom, int iRight);

/*
Rotates the given image with the given angle, pivoting from the center
of the image, and returns the rotated image as a new image
	- IMAGE* imgPtr: Image you want to rotate
	- float theta: How much in radians you want to rotate by
*/
IMAGE RotateImage(IMAGE * imgPtr, float theta);

/* Function Pointers */
/* Processes the given coordinates in a specific fashion.
   Can be used to make the resulting Image bigger/smaller
   depending on the function (ex. Modifying RotateImage so
   that the whole image is shown and not cut off)
*/
typedef BOOL(*PROCESS_COORDS) (IMAGE*, int*, int*, int*, int*);

/* Helper Methods */

/* Calculates the amount of padding for a given image */
int GetPadding(IMAGE* imgPtr);
/* Calculates the row size for a given image */
int GetRowSize(IMAGE* imgPtr, BOOL includePadding);


/* A helper method that prepares the header of a given image
	- IMAGE * imOld: Pointer to original image
	- IMAGE * imOld: Pointer to new image
	- int* iTop: Pointer to position from the top of the image. If NULL is specified,
	than nothing will happen to the coordinates.
	- int* iLeft: Pointer to position from the left of the image. If NULL is specified,
	than nothing will happen to the coordinates.
	- int* iBottom: Pointer to position from the top of the image where the image will
	be cut off (should be greater than iTop). If NULL is specified,
	than nothing will happen to the coordinates.
	- int* iRight: Pointer to position from the left of the image where the image will
	be cut off (should be greater than iLeft). If NULL is specified,
	than nothing will happen to the coordinates.
	- PROCESS_COORDS coordFunc: A pointer to a function on how to deal with the specified
	coordinates shown above. If NULL is specified, than nothing
	will happen to the coordinates.
*/
BOOL PrepareImage(IMAGE* imOld, IMAGE* imNew, int* iTop,
	int* iLeft, int* iBottom, int* iRight, PROCESS_COORDS coordFunc);

/* A helper method that prepares the header of a given image.
   Doesn't do anything with coordinates, so resulting image will
   stay the same size as the original.
	- IMAGE * imOld: Pointer to original image
	- IMAGE * imNew: Pointer to new image
*/
BOOL PrepareImageNoCoords(IMAGE* imOld, IMAGE* imNew);

/* Helper method for Crop: ensures the given coordinates are within the image's bounds */
BOOL CheckCoords(IMAGE * imgPtr, int* iTop, int* iLeft, int* iBottom, int* iRight);

/* Help method to find clamp a given value within the given range */
int clamp(int current, int min, int max);

/* Helper method for RotateImage: takes a coordinate and blends the 4 pixels around
   the coordinate and returns a "combined" pixel
 */
PIXEL GetBlendedPixel(IMAGE* img, double x, double y);

#pragma pack (pop)
#endif // !BITMAP_H

