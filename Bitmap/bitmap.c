#include "bitmap.h"
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
/*
Author: Corey Janzen
Date: Apr 11, 2018
*/

/* The default background color used in the rotations to fill empty space */
#define BACK_COLOR { 255, 0, 0 }

/* Retrieves a reference to a single pixel in the image */
PIXEL* GetPixel(unsigned int row, unsigned int col, IMAGE* imgPtr)
{
	// Calculate the padding
	unsigned int padding = imgPtr->bmHDR->dwWidth % 4;
	// Calculate the total offset
	unsigned int offset = row * (imgPtr->bmHDR->dwWidth * PIXELSIZE + padding) + col * PIXELSIZE;

	return (PIXEL*)((BYTE*)imgPtr->bmData + offset);
}

/* Gets a file handle from the user */
FILE * GetFile(char * cPrompt, char * cMode)
{
	FILE* aFile = NULL;
	char cFileName[256];
	printf("%s: \n", cPrompt);
	scanf("%s", cFileName);
	// Open the file with the file mode passed in
	aFile = fopen(cFileName, cMode);
	return aFile;
}

/* Reads in a whole Bitmap file */
void ReadImage(IMAGE * imgPtr, FILE * infile)
{
	// Make sure the image struct exists and file pointer is not null
	if (imgPtr != NULL && infile != NULL)
	{
		// Read the header
		ReadHeader(imgPtr, infile);
		// Check if the header was read in
		if (imgPtr->bmHDR != NULL)
		{
			// Read in the data
			ReadData(imgPtr, infile);
		}
	}
}

/* Reads in bitmap header */
void ReadHeader(IMAGE * imgPtr, FILE * infile)
{
	BOOL bSuccess = FALSE;
	// Allocate memory and check if malloc was successful
	if ((imgPtr->bmHDR = (BITMAPHDR*)malloc(BMPHDRSIZE)) != NULL)
	{
		if (fread(imgPtr->bmHDR, BMPHDRSIZE, 1, infile) == 1)
		{
			bSuccess = TRUE;
		}
	}

	// Did the read work or not?
	if (!bSuccess && imgPtr->bmHDR != NULL)
	{
		free(imgPtr->bmHDR);
		// Indicate that we were not successful
		imgPtr->bmHDR = NULL;
	}
}

/* Reads in bitmap data */
void ReadData(IMAGE * imgPtr, FILE * infile)
{
	BOOL bSuccess = FALSE;
	// The size of the image in bytes and the amount of padding
	unsigned int imageSize = 0, padding = 0;

	// Calculate the padding
	padding = imgPtr->bmHDR->dwWidth % 4;

	// Calculate the image size = # of rows * sizeof each row
	imageSize = imgPtr->bmHDR->dwHeight * (PIXELSIZE * imgPtr->bmHDR->dwWidth + padding);

	// Allocate memory for the data
	if ((imgPtr->bmData = (PIXEL*)malloc(imageSize)) != NULL)
	{
		// Read in the pixels
		if ((fread(imgPtr->bmData, imageSize, 1, infile) == 1))
		{
			bSuccess = TRUE;
		}
	}

	// If we failed
	if (!bSuccess)
	{
		// Free header
		if (imgPtr->bmHDR != NULL)
		{
			free(imgPtr->bmHDR);
			imgPtr->bmHDR = NULL;
		}

		// Free the data
		if (imgPtr->bmData != NULL)
		{
			free(imgPtr->bmData);
			imgPtr->bmData = NULL;
		}
	}
}

/* Writes a whole bitmap to file */
void WriteImage(IMAGE * imgPtr, FILE * outfile)
{
	if (imgPtr != NULL && outfile != NULL)
	{
		// Write the header out
		WriteHeader(imgPtr, outfile);
		// Check if header wrote out
		if (outfile != NULL)
		{
			WriteData(imgPtr, outfile);
		}

	}
}

/* Writes a bitmap header to file */
void WriteHeader(IMAGE * imgPtr, FILE * outfile)
{
	BOOL bSuccess = FALSE;
	if (imgPtr->bmHDR != NULL)
	{
		if (fwrite(imgPtr->bmHDR, BMPHDRSIZE, 1, outfile) == 1)
		{
			bSuccess = TRUE;
		}
	}

	// If we failed
	if (!bSuccess && outfile != NULL)
	{
		fclose(outfile);
		outfile = NULL;
	}
}

/* Writes out bitmap data to file */
void WriteData(IMAGE * imgPtr, FILE * outfile)
{
	BOOL bSuccess = FALSE;
	unsigned int imageSize = 0, padding = 0;
	padding = imgPtr->bmHDR->dwWidth % 4;

	// If the header and data exist in the image
	if (imgPtr->bmHDR != NULL && imgPtr->bmData != NULL)
	{
		// Calculate the image size = # rows * size of a row
		imageSize = imgPtr->bmHDR->dwHeight * (imgPtr->bmHDR->dwWidth * PIXELSIZE + padding);
		// Write the data to file
		if (fwrite(imgPtr->bmData, imageSize, 1, outfile) == 1)
		{
			bSuccess = TRUE;
		}
	}
	// If we fail
	if (!bSuccess && outfile != NULL)
	{
		fclose(outfile);
		outfile = NULL;
	}
}

/* Free's Bitmap's inner members */
void FreeImage(IMAGE * img)
{
	// Free the image
	if (img->bmHDR) free(img->bmHDR);
	img->bmHDR = NULL;
	if (img->bmData) free(img->bmData);
	img->bmData = NULL;
}

/* Calculates the amount of padding for a given image */
int GetPadding(IMAGE * imgPtr)
{
	// Make sure the image exists and has the proper data
	// for the calculation
	if (!imgPtr || !imgPtr->bmHDR) return 0; // 0 Should be default

	// If image and header are present, calculate the padding
	return imgPtr->bmHDR->dwWidth % 4;
}

/* Calculates the row size for a given image */
int GetRowSize(IMAGE * imgPtr, BOOL includePadding)
{
	// Make sure the image exists and has the proper data
	// for the calculation
	if (!imgPtr || !imgPtr->bmHDR) return 0; // 0 Should be default

	// If image and header are present, calculate the row size
	return imgPtr->bmHDR->dwWidth * PIXELSIZE + (includePadding ? GetPadding(imgPtr) : 0);
}

/* A helper method that prepares the header of a given image
	- IMAGE * imOld: Pointer to original image
	- IMAGE * imOld: Pointer to new image
	- int* iTop: Pointer to position from the top of the image. If NULL is specified,
			     than nothing will happen to the coordinates.
	- int* iLeft: Pointer to position from the left of the image. If NULL is specified,
			      than nothing will happen to the coordinates.
	- int* iBottom: Pointer to position from the top of the image where the image will
					be cut off (should be greater than iTop). If NULL is specified,
			        than nothing will happen to the coordinates.
	- int* iRight: Pointer to position from the left of the image where the image will
				   be cut off (should be greater than iLeft). If NULL is specified,
			       than nothing will happen to the coordinates.
	- PROCESS_COORDS coordFunc: A pointer to a function on how to deal with the specified
								 coordinates shown above. If NULL is specified, than nothing
								 will happen to the coordinates.
*/
BOOL PrepareImage(IMAGE * imOld, IMAGE * imNew, int * iTop,
	int * iLeft, int * iBottom, int * iRight, PROCESS_COORDS coordFunc)
{
	// Success indicator for the entire function call
	BOOL bSuccess = FALSE;
	// Success indicator for the call to function pointer
	BOOL bFuncSuccess = FALSE;

	// If the old and new Images exists and has the data we need
	if (imOld && imNew && imOld->bmHDR && imOld->bmData)
	{
		// Prepare the header for the new image by processing
		// specified coordinates (if process was specified)
		bFuncSuccess = coordFunc != NULL ? coordFunc(imOld, iTop, iLeft, iBottom, iRight) : TRUE;

		// If function was successful
		if (bFuncSuccess)
		{
			// Allocate the header for the new image
			imNew->bmHDR = (BITMAPHDR*)malloc(BMPHDRSIZE);

			// If allocation was successful
			if (imNew->bmHDR)
			{
				// Copy header from old image to new image
				if (memcpy(imNew->bmHDR, imOld->bmHDR, BMPHDRSIZE))
				{
					// Set the width and height of the Image appropriately (if necessary)
					if (iLeft && iTop && iRight && iBottom)
					{
						imNew->bmHDR->dwWidth = *iRight - *iLeft;
						imNew->bmHDR->dwHeight = *iBottom - *iTop;
					}

					// Get the size of the new image's data
					int iImgSize = imNew->bmHDR->dwHeight * GetRowSize(imNew, TRUE);
					// Calculate new file size
					imNew->bmHDR->dwFileSize = BMPHDRSIZE + iImgSize;
					// Allocate memory for the new Image's data
					imNew->bmData = (PIXEL*)malloc(iImgSize);

					// Set success indicator based on whether or not alloc
					// was successful
					bSuccess = imNew->bmData != NULL;
				}
			}
		}
	}

	// If unsuccessful, free any memory that was allocated for new image
	if (!bSuccess)
	{
		if (imNew->bmHDR) free(imNew->bmHDR);
		imNew->bmHDR = NULL;
		if (imNew->bmData) free(imNew->bmData);
		imNew->bmData = NULL;
	}

	// Return success indicator
	return bSuccess;
}

/* A helper method that prepares the header of a given image.
   Doesn't do anything with coordinates, so resulting image will
   stay the same size as the original.
- IMAGE * imOld: Pointer to original image
- IMAGE * imNew: Pointer to new image
*/
BOOL PrepareImageNoCoords(IMAGE * imOld, IMAGE * imNew)
{
	// Just call PrepareImage with coordinates and coordFunc set to NULL
	return PrepareImage(imOld, imNew, NULL, NULL, NULL, NULL, NULL);
}

/* Helper method for Crop: ensures the given coordinates are within the image's bounds */
BOOL CheckCoords(IMAGE * imgPtr, int* iTop, int* iLeft, int* iBottom, int* iRight)
{
	// Fix the coordinates if they are out of bounds	
	*iTop = clamp(*iTop, 0, imgPtr->bmHDR->dwHeight);	
	*iBottom = clamp(*iBottom, 0, imgPtr->bmHDR->dwHeight);
	*iLeft = clamp(*iLeft, 0, imgPtr->bmHDR->dwWidth);
	*iRight = clamp(*iRight, 0, imgPtr->bmHDR->dwWidth);

	// Check if there's at least 1 pixel to crop
	return *iBottom - *iTop > 0 && *iRight - *iLeft > 0;
}

/* Help method to find clamp a given value within the given range */
int clamp(int current, int min, int max)
{
	const int t = current < min ? min : current;
	return t > max ? max : t;
}

/* Helper method for RotateImage: takes a coordinate and blends the 4 pixels around
the coordinate and returns a "combined" pixel
*/
PIXEL GetBlendedPixel(IMAGE * img, double x, double y)
{
	// Blended pixel to return
	PIXEL pBlended = { 0, 0, 0 };
	// Find the "highest" pixel of each image
	int iImgHeight = img->bmHDR->dwHeight - 1;
	// Coordinates for the 4 pixels, where "t" means truncated, while "c"
	// means ceiled
	int xt = clamp((int)x, 0, img->bmHDR->dwWidth - 1),
		yt = iImgHeight - clamp((int)y, 0, img->bmHDR->dwHeight - 1),
		xc = clamp((int)ceil(x), 0, img->bmHDR->dwWidth - 1),
		yc = iImgHeight - clamp((int)ceil(y), 0, img->bmHDR->dwHeight - 1);

	// Get the four pixel around the specified coordinates
	PIXEL p1 = *GetPixel(yt, xt, img);
	PIXEL p2 = *GetPixel(yt, xc, img);
	PIXEL p3 = *GetPixel(yc, xt, img);
	PIXEL p4 = *GetPixel(yc, xc, img);

	// Blend the pixels together
	pBlended.bRed = (p1.bRed + p2.bRed + p3.bRed + p4.bRed) / 4;
	pBlended.bGrn = (p1.bGrn + p2.bGrn + p3.bGrn + p4.bGrn) / 4;
	pBlended.bBlue = (p1.bBlue + p2.bBlue + p3.bBlue + p4.bBlue) / 4;
	
	return pBlended;
}

/*
Crops the specified image using the specified coordinates and returns
a new cropped image.
	- IMAGE * imgPtr: Image you want to crop
	- int iTop: position from the top of the image
	- int iLeft: Position from the left of the image
	- int iBottom: position from the top of the image where the image will
				   be cut off (should be greater than iTop)
	- int iRight: position from the left of the image where the image will
				  be cut off (should be greater than iLeft)
*/
IMAGE Crop(IMAGE * imgPtr, int iTop, int iLeft, int iBottom, int iRight)
{
	// y coordinates of the image
	int y = 0;
	// The new image to return
	IMAGE Image = {NULL, NULL};
	// Prepare the new Image for the crop
	BOOL bSuccess = PrepareImage(imgPtr, &Image, &iTop, &iLeft,
		&iBottom, &iRight, CheckCoords);

	// If preparation was successful
	if (bSuccess)
	{
		// Find the "highest" pixel of each image
		int iOldHeight = imgPtr->bmHDR->dwHeight - 1;
		int iNewHeight = Image.bmHDR->dwHeight - 1;
		// For each row of data
		for (y = iTop; y < iBottom && bSuccess; y++)
		{
			// Get pointer to beginning of row of both images
			PIXEL* oldRowPtr = GetPixel(iOldHeight - y, iLeft, imgPtr);
			PIXEL* newRowPtr = GetPixel(iNewHeight - (y - iTop), 0, &Image);
		
			// Use memcpy to copy the data over for the current row
			memcpy(newRowPtr, oldRowPtr, Image.bmHDR->dwWidth * PIXELSIZE);
		}
	}

	// Return the created image
	return Image;
}

/*
Rotates the given image with the given angle, pivoting from the center
of the image, and returns the rotated image as a new image
- IMAGE* imgPtr: Image you want to rotate
- float theta: How much in radians you want to rotate by
*/
IMAGE RotateImage(IMAGE * imgPtr, float theta)
{
	// coordinates of the new image
	int x = 0, y = 0;
	// new x, y coords from old image
	double xi = 0, yi = 0;
	// The pivot point
	double px = 0, py = 0;
	// Default background color
	PIXEL pBack = BACK_COLOR;
	// Reverse theta, since we are looping through destination rather than source
	theta = -theta;

	// The new image to return
	IMAGE Image = { NULL, NULL };
	// Prepare the new Image for the crop
	BOOL bSuccess = PrepareImageNoCoords(imgPtr, &Image);

	// If preparation was successful
	if (bSuccess)
	{
		// Find the "highest" pixel of each image
		int iImgHeight = Image.bmHDR->dwHeight - 1;
		// Get pivot point of the image
		px = Image.bmHDR->dwWidth / 2;
		py = Image.bmHDR->dwHeight / 2;
		
		// For each row
		for (y = 0; y < imgPtr->bmHDR->dwHeight; y++)
		{
			// for each column
			for (x = 0; x < imgPtr->bmHDR->dwWidth; x++)
			{
				// Get pointer to the current pixel in new image
				PIXEL* pNew = GetPixel(iImgHeight - y, x, &Image);

				// Find the pixel in the old image that cooresponds to the current
				// pixel in the new image
				xi = (x - px) * cos(theta) - (y - py) * sin(theta) + px;
				yi = (x - px) * sin(theta) + (y - py) * cos(theta) + py;

				// Make sure the pixels are in range of the image's bounds
				if (xi >= 0 && xi < imgPtr->bmHDR->dwWidth &&
					yi >= 0 && yi < imgPtr->bmHDR->dwHeight)
				{
					// Get a blended pixel from the calculated position
					PIXEL pBlended = GetBlendedPixel(imgPtr, xi, yi);
					*pNew = pBlended;
				}
				else
				{
					// Insert background color
					*pNew = pBack;
				}
			}
		}
	}

	// Return the created image
	return Image;
}
